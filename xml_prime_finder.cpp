#include "xml_prime_finder.h"

XmlPrimeFinder::XmlPrimeFinder(DummyXml const & xml) {
	this->add_interval(xml);
}

void XmlPrimeFinder::add_interval(DummyXml const & xml) {
	for (auto const & intervals : xml.get_elements()) {
		if (intervals.get_name() != "intervals")
			continue ;
		for (auto const & interval : intervals.get_elements()) {
			if (interval.get_name() != "interval")
				continue ;
			unsigned int low, high;
			bool low_flag = false, high_flag = false;
			for (auto const & value : interval.get_elements()) {
				if (value.get_name() == "low") {
					if (value.get_text().find('-') != std::string::npos)
						throw std::invalid_argument("Value can't be negative.");
					low = std::stoul(value.get_text());
					low_flag = true;
				}
				else if (value.get_name() == "high") {
					if (value.get_text().find('-') != std::string::npos)
						throw std::invalid_argument("Value can't be negative.");
					high = std::stoul(value.get_text());
					high_flag = true;
				}
			}
			if (!low_flag || !high_flag)
				throw std::invalid_argument(
						"One of interval's bounds is not set.");;
			PrimeFinder::add_interval(low, high);
		}
	}
}

DummyXml XmlPrimeFinder::get_results() {
	DummyXml root("root"), intervals("intervals");
	for (auto const & it : PrimeFinder::get_results()) {
		DummyXml interval("interval"), low("low"), high("high"), primes("primes");
		low.set_text(std::to_string(it.first.first));
		high.set_text(std::to_string(it.first.second));
		std::string primes_str;
		for (auto const & prime_it : it.second)
			primes_str += ((primes_str.empty()) ? "" : " ") + std::to_string(prime_it);
		primes.set_text(primes_str);
		interval.add_element(low);
		interval.add_element(high);
		interval.add_element(primes);
		intervals.add_element(interval);
	}
	root.add_element(intervals);
	return root;
}